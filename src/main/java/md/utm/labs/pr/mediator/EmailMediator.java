package md.utm.labs.pr.mediator;

import java.io.File;
import java.util.List;

import md.utm.labs.pr.controller.MainController;
import md.utm.labs.pr.email.Email;
import md.utm.labs.pr.email.EmailClient;

public interface EmailMediator {
	void addEmail(Email email);
	void clearEmails();
	Email getEmail();
	EmailClient getSmtpEmailClient();
	EmailClient getPop3EmailClient();
	void addFileToAttach(File file);
	List<File> getAttachedFiles();

	
	public static EmailMediator getMediator() {
		return MainController.getInstance();
	}
}
