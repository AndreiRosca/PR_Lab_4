package md.utm.labs.pr.controller;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ListView;

public class EmailMetadataController implements Initializable {

	private final List<String> listItems;

	public EmailMetadataController(List<String> listItems) {
		this.listItems = listItems;
	}

	@FXML
	private ListView<String> emailHeadersListView;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		emailHeadersListView.getItems().addAll(listItems);
	}
}
