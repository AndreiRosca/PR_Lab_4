package md.utm.labs.pr.controller;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextArea;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleGroup;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import md.utm.labs.pr.email.Email;

public class ViewEmailController implements Initializable {
	
	private final Email email;

	@FXML
	private TextArea emailContentTextArea;
	
	@FXML
	private WebView emailContentWebView;
	
	@FXML
	private RadioButton textRadioButton;
	
	@FXML
	private RadioButton htmlRadioButton;
	
	@FXML
	private Button viewHeadersButton;
	
	@FXML Button viewAttachmentsButton;
	
	private ToggleGroup group;
	
	public ViewEmailController(Email email) {
		this.email = email;
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		viewHeadersButton.setOnAction(new ViewEmailEventHandler());
		viewAttachmentsButton.setOnAction(new ViewEmailAttachmentsHandler());
		setUpToggleGroup();
		if (email.canGetMessageAsHtml())
			showEmailAsHtml();
		else
			showEmailAsPlainText();
	}

	private void setUpToggleGroup() {
		textRadioButton.setUserData("text/plain");
		htmlRadioButton.setUserData("text/html");
		group = new ToggleGroup();
		group.selectedToggleProperty().addListener(new MediaTypeChangedListener());
		textRadioButton.setToggleGroup(group);
		htmlRadioButton.setToggleGroup(group);
	}
	
	private void showEmailAsHtml() {
		emailContentTextArea.setVisible(false);
		emailContentWebView.setVisible(true);
		emailContentWebView.getEngine().loadContent(email.getMessageAsHtml());
	}
	
	private void showEmailAsPlainText() {
		emailContentTextArea.setVisible(true);
		emailContentWebView.setVisible(false);
		emailContentTextArea.setText(email.getMessageAsText());
	}
	
	private class ViewEmailEventHandler implements EventHandler<ActionEvent> {
		public void handle(ActionEvent event) {
			String layoutFile = "/viewEmailHeadersLayout.fxml";
			String windowTitle = "View email headers";
			createWindow(layoutFile, new EmailMetadataController(prepareHeaders()), windowTitle);
		}
		
		private List<String> prepareHeaders() {
			List<String> headers = new ArrayList<>();
			for (Map.Entry<String, String> entry : email.getHeaders().entrySet()) {
				String header = entry.getKey() + ": " + entry.getValue();
				headers.add(header);
			}
			return headers;
		}
	}
	
	private void createWindow(String layoutFile, Object controller, String title) {
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setController(controller);
			loader.load(getClass().getResourceAsStream(layoutFile));
			Parent parent = loader.getRoot();
			Stage primaryStage = new Stage();
			primaryStage.setScene(new Scene(parent));
			primaryStage.setTitle(title);
			primaryStage.show();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
	
	private class ViewEmailAttachmentsHandler implements EventHandler<ActionEvent> {
		public void handle(ActionEvent event) {
			String layoutFile = "/viewEmailAttachmentsLayout.fxml";
			String windowTitle = "View email attachments";
			createWindow(layoutFile, new EmailAttachmentsController(email), windowTitle);
		}
	}
	
	private class MediaTypeChangedListener implements ChangeListener<Toggle> {

		@Override
		public void changed(ObservableValue<? extends Toggle> observable, Toggle oldValue, Toggle newValue) {
			if (group.getSelectedToggle() != null) {
				String userData = group.getSelectedToggle().getUserData().toString();
				if (email.canGetMessageAsHtml() && userData.contains("text/html"))
					showEmailAsHtml();
				else
					showEmailAsPlainText();
	        }
		}
	}
}
