package md.utm.labs.pr.controller;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;

import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ListView;
import javafx.scene.input.MouseEvent;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import md.utm.labs.pr.email.Email;
import md.utm.labs.pr.email.util.EmailEntity;

public class EmailAttachmentsController implements Initializable {

	private final Email email;

	@FXML
	private ListView<String> emailAttachmentsListView;

	public EmailAttachmentsController(Email email) {
		this.email = email;
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		emailAttachmentsListView.setOnMouseClicked(new DoubleClickEventHandler());
		for (EmailEntity e : email.getAttachedFiles()) {
			emailAttachmentsListView.getItems().add(e.getAttachedFileName());
		}
	}

	private class DoubleClickEventHandler implements EventHandler<MouseEvent> {

		@Override
		public void handle(MouseEvent event) {
			if (event.getClickCount() == 2) {
				String attachedFile = emailAttachmentsListView.getSelectionModel().getSelectedItem();
				Optional<EmailEntity> entity = getFileContent(attachedFile);
				if (entity.isPresent()) {
					byte[] fileContent = entity.get().getAttachedFileContent();
					chooseFile().ifPresent(f -> writeFileToDisk(f, fileContent));
				}
			}
		}
		
		private void writeFileToDisk(File file, byte[] fileContent) {
			try (BufferedOutputStream out = new BufferedOutputStream(new FileOutputStream(file))) {
				out.write(fileContent, 0, fileContent.length);
			} catch (Exception e) {
				throw new RuntimeException(e);
			}
		}

		private Optional<File> chooseFile() {
			FileChooser chooser = new FileChooser();
			chooser.setTitle("Open file to attach");
			File file = chooser.showSaveDialog(new Stage());
			return file != null ? Optional.of(file) : Optional.empty();
		}

		private Optional<EmailEntity> getFileContent(String attachedFile) {
			 return email.getAttachedFiles()
					 .stream()
					.filter(e -> e.getAttachedFileName().equals(attachedFile))
					.findFirst();
		}
	}
}
