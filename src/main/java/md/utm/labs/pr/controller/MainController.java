package md.utm.labs.pr.controller;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;

import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import md.utm.labs.pr.email.Email;
import md.utm.labs.pr.email.EmailClient;
import md.utm.labs.pr.handler.MenuEventHandlerRegistry;
import md.utm.labs.pr.handler.factory.MenuEventHandlerFactory;
import md.utm.labs.pr.mediator.EmailMediator;
import md.utm.labs.pr.threads.DaemonThreadFactory;

public class MainController implements Initializable, EmailMediator {
	private static final int MAX_THREADS = 4;
	private final ExecutorService executor = 
			Executors.newFixedThreadPool(MAX_THREADS, new DaemonThreadFactory());
	private static AtomicInteger emailCounter = new AtomicInteger();
	private static final Map<Integer, PropertyValueFactory<Email, String>> columnFactories = 
			new HashMap<>();
	private static final MainController instance = new MainController();
	private static final MenuEventHandlerRegistry eventHandlerRegistry = 
			MenuEventHandlerRegistry.getInstance();

	private List<File> attachedFiles = new ArrayList<>();
	
	static {
		columnFactories.put(0, new PropertyValueFactory<>("id"));
		columnFactories.put(1, new PropertyValueFactory<>("from"));
		columnFactories.put(2, new PropertyValueFactory<>("subject"));
		columnFactories.put(3, new PropertyValueFactory<>("date"));
	}
	
	@FXML
	private TextField smtpServerTextField;
	
	@FXML
	private TextField smtpPortTextField;
	
	@FXML
	private TextField usernameTextField;
	
	@FXML
	private PasswordField passwordField;
	
	@FXML
	private TextField subjectTextField;
	
	@FXML
	private TextField fromTextField;
	
	@FXML
	private TextField toTextField;
	
	@FXML
	private Button sendButtom;
	
	@FXML
	private RadioButton plainTextRadionButton;
	
	@FXML
	private RadioButton htmlRadioButton;
	
	private ToggleGroup group;
	
	//
	
	@FXML
	private TextField pop3ServerTextField;
	
	@FXML
	private TextField pop3PortTextField;
	
	@FXML
	private TextField pop3UsernameTextField;
	
	@FXML
	private PasswordField pop3PasswordField;
	
	@FXML
	private TextArea messageTextArea;
	
	@FXML
	private TableView<Email> emailListTableView;
	
	@FXML
	private Button refreshEmailListButton;
	
	@FXML
	private Button attachFileButton;
	
	@FXML
	private Button viewAttachedFilesButton;
	
	private MainController() {
	}

	public static MainController getInstance() {
		return instance;
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		MenuEventHandlerFactory.makeHandlers();
		registerMenuHandlers();
		setUpToggleGroup();
		int i = 0;
		for (TableColumn column : emailListTableView.getColumns()) {
			column.setCellValueFactory(columnFactories.get(i++));
		}
	}

	private void setUpToggleGroup() {
		plainTextRadionButton.setUserData("text/plain");
		htmlRadioButton.setUserData("text/html");
		group = new ToggleGroup();
		plainTextRadionButton.setToggleGroup(group);
		htmlRadioButton.setToggleGroup(group);
	}
	
	private void registerMenuHandlers() {
		refreshEmailListButton.setOnAction(eventHandlerRegistry);
		attachFileButton.setOnAction(eventHandlerRegistry);
		viewAttachedFilesButton.setOnAction(eventHandlerRegistry);
		sendButtom.setOnAction(eventHandlerRegistry);
		emailListTableView.setOnMousePressed(new EventHandler<MouseEvent>() {
			public void handle(MouseEvent event) {
				if (event.isPrimaryButtonDown() && event.getClickCount() == 2) {
					Node node = ((Node) event.getTarget()).getParent();
		            TableRow<Email> row;
		            if (node instanceof TableRow) {
		                row = (TableRow) node;
		            } else {
		                row = (TableRow) node.getParent();
		            }
		            Email email = row.getItem();
		            showEmailWindow(email);
				}
			}

			private void showEmailWindow(Email email) {
				try {
					FXMLLoader loader = new FXMLLoader();
					loader.setController(new ViewEmailController(email));
					loader.load(getClass().getResourceAsStream("/viewEmailLayout.fxml"));
					Parent parent = loader.getRoot();
					Stage primaryStage = new Stage();
					primaryStage.setScene(new Scene(parent));
					primaryStage.setTitle("View email");
					primaryStage.show();
				} catch (IOException e) {
					throw new RuntimeException(e);
				}
			}
		});
	}

	@Override
	public synchronized void addEmail(Email email) {
		Platform.runLater(() -> {
			email.setId(emailCounter.incrementAndGet());
			emailListTableView.getItems().add(email);
			if (email.getId() % 10 == 0)
				emailListTableView.scrollTo(email);
		});
	}

	@Override
	public void clearEmails() {
		emailCounter = new AtomicInteger();
		emailListTableView.getItems().clear();
	}

	@Override
	public Email getEmail() {
		Email e =  Email.newBuilder()
				.setFrom(fromTextField.getText())
				.setTo(toTextField.getText())
				.setSubject(subjectTextField.getText())
				.setMessage(messageTextArea.getText())
				.setAttachedFiles(attachedFiles)
				.setContentType(group.getSelectedToggle().getUserData().toString())
				.build();
		attachedFiles.clear();
		return e;
	}

	@Override
	public EmailClient getSmtpEmailClient() {
		return EmailClient.newBuilder()
				.setExecutor(executor)
				.setUsername(usernameTextField.getText())
				.setPassword(passwordField.getText())
				.setSmtpAddress(smtpServerTextField.getText())
				.setSmtpPort(Integer.valueOf(smtpPortTextField.getText()))
				.build();
	}

	@Override
	public EmailClient getPop3EmailClient() {
		return EmailClient.newBuilder()
				.setExecutor(executor)
				.setPop3Address(pop3ServerTextField.getText())
				.setPop3Port(Integer.valueOf(pop3PortTextField.getText()))
				.setUsername(pop3UsernameTextField.getText())
				.setPassword(pop3PasswordField.getText())
				.build();
	}

	@Override
	public void addFileToAttach(File file) {
		attachedFiles.add(file);
	}

	@Override
	public List<File> getAttachedFiles() {
		return Collections.unmodifiableList(attachedFiles);
	}
}
