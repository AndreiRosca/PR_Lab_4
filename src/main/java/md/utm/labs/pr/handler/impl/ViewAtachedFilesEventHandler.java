package md.utm.labs.pr.handler.impl;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import md.utm.labs.pr.controller.EmailMetadataController;
import md.utm.labs.pr.handler.BaseMenuEventHandler;

public class ViewAtachedFilesEventHandler extends BaseMenuEventHandler {

	public ViewAtachedFilesEventHandler() {
		super("viewAttachedFilesButton");
	}

	@Override
	public void handle() {
		List<File> attachedFiles = getMediator().getAttachedFiles();
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setController(new EmailMetadataController(prepareFiles(attachedFiles)));
			loader.load(getClass().getResourceAsStream("/viewEmailHeadersLayout.fxml"));
			Parent parent = loader.getRoot();
			Stage primaryStage = new Stage();
			primaryStage.setScene(new Scene(parent));
			primaryStage.setTitle("View attached files");
			primaryStage.show();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	private List<String> prepareFiles(List<File> attachedFiles) {
		return attachedFiles.stream()
				.map(f -> f.toString())
				.collect(Collectors.toList());
	}
}
