package md.utm.labs.pr.handler.impl;

import java.util.List;

import md.utm.labs.pr.email.Email;
import md.utm.labs.pr.email.EmailClient;
import md.utm.labs.pr.email.EmailScan;
import md.utm.labs.pr.handler.BaseMenuEventHandler;

public class RefreshEmailListEventHandler extends BaseMenuEventHandler  {

	public RefreshEmailListEventHandler() {
		super("refreshEmailListButton");
	}

	@Override
	public void handle() {
		getMediator().clearEmails();
		EmailClient client = getMediator().getPop3EmailClient();
		client.getEmails()
			  .thenAccept(emails -> processEmailList(emails, client));
	}

	private void processEmailList(List<EmailScan> emails, EmailClient client) {
		for (EmailScan scan : emails) {
			client.getEmail(scan.getIndex())
				  .thenAccept(e -> {
					  try {
					  Email email = Email.from(e);
					  getMediator().addEmail(email);
					  } catch (Exception ex) {
						 throw new RuntimeException(e);
					  }
				  });
		}
	}
}
