package md.utm.labs.pr.handler.impl;

import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import md.utm.labs.pr.email.Email;
import md.utm.labs.pr.email.EmailClient;
import md.utm.labs.pr.handler.BaseMenuEventHandler;

public class SendEmailEventHandler extends BaseMenuEventHandler {

	public SendEmailEventHandler() {
		super("sendButtom");
	}

	@Override
	public void handle() {
		Email email = getMediator().getEmail();
		EmailClient client = getMediator().getSmtpEmailClient();
		client.sendEmail(email);
		showSuccessDialog();
	}

	private void showSuccessDialog() {
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("Notification");
		alert.setHeaderText("The email was successfully sent!");
		alert.setContentText("Email status");
		alert.showAndWait();
	}
}
