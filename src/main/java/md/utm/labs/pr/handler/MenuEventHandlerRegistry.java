package md.utm.labs.pr.handler;

import java.util.HashMap;
import java.util.Map;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Node;

public class MenuEventHandlerRegistry implements EventHandler<ActionEvent> {
	
	private static final Map<String, MenuEventHandler> handlers = new HashMap<>();
	private static final MenuEventHandlerRegistry instance = new MenuEventHandlerRegistry();
	
	private MenuEventHandlerRegistry() {
	}
	
	public static MenuEventHandlerRegistry getInstance() {
		return instance;
	}

	@Override
	public void handle(ActionEvent event) {
		Node item = (Node) event.getSource();
		handlers.getOrDefault(item.getId(), NULL_HANDLER).handle();
	}
	
	public void registerHandler(String id, MenuEventHandler handler) {
		handlers.put(id, handler);
	}

	private static final MenuEventHandler NULL_HANDLER = new MenuEventHandler() {
		public void handle() {
			System.err.println("Null event handler fallback");
		}
	};
}
