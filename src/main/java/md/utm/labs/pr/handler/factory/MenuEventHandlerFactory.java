package md.utm.labs.pr.handler.factory;

import md.utm.labs.pr.handler.impl.*;

public class MenuEventHandlerFactory {

	public static void makeHandlers() {
		new RefreshEmailListEventHandler();
		new SendEmailEventHandler();
		new AttachFileEventHandler();
		new ViewAtachedFilesEventHandler();
	}
}
