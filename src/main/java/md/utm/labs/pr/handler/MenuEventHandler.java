package md.utm.labs.pr.handler;

import md.utm.labs.pr.mediator.EmailMediator;

public interface MenuEventHandler {
	void handle();
	
	public default EmailMediator getMediator() {
		return EmailMediator.getMediator();
	}
}
