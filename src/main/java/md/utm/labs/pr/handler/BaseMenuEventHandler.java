package md.utm.labs.pr.handler;

public abstract class BaseMenuEventHandler implements MenuEventHandler {
	
	public BaseMenuEventHandler(String eventId) {
		MenuEventHandlerRegistry registry = MenuEventHandlerRegistry.getInstance();
		registry.registerHandler(eventId, this);
	}
}
