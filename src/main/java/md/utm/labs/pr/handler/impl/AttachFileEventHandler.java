package md.utm.labs.pr.handler.impl;

import java.io.File;

import javafx.stage.FileChooser;
import javafx.stage.Stage;
import md.utm.labs.pr.handler.BaseMenuEventHandler;

public class AttachFileEventHandler extends BaseMenuEventHandler {

	public AttachFileEventHandler() {
		super("attachFileButton");
	}

	@Override
	public void handle() {
		FileChooser chooser = new FileChooser();
		chooser.setTitle("Open file to attach");
		File file = chooser.showOpenDialog(new Stage());
		if (file == null)
			return;
		getMediator().addFileToAttach(file);
	}
}
