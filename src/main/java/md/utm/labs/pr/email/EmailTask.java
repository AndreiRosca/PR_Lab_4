package md.utm.labs.pr.email;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.concurrent.CompletableFuture;

public abstract class EmailTask<T> implements Runnable {
	protected final CompletableFuture<T> future = new CompletableFuture<T>();
	protected final EmailClient client;

	public EmailTask(EmailClient client) {
		this.client = client;
	}

	public CompletableFuture<T> execute() {
		client.getExecutor().submit(this);
		return future;
	}
	
	public void run() {
		try (Socket socket = new Socket(client.getPop3Address(), client.getPop3Port())) {
			PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
			try (BufferedReader reader = 
					new BufferedReader(new InputStreamReader(socket.getInputStream()))) {
				readStatusLine(reader);
				write(out, "USER " + client.getUsername());
				readStatusLine(reader);
				write(out, "PASS " + client.getPassword());
				readStatusLine(reader);
				executePop3Command(reader, out);
				write(out, "QUIT");
			} finally {
				out.close();
			}
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	protected abstract void executePop3Command(BufferedReader reader, PrintWriter out) 
			throws IOException;

	protected String readStatusLine(BufferedReader reader) throws IOException {
		String line = reader.readLine();
		if (line.startsWith("-"))
			throw new RuntimeException(line.substring(1));
		return line;
	}
	
	protected void write(PrintWriter out, String message) {
		out.print(message + "\r\n");
		out.flush();
	}
}
