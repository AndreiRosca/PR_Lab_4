package md.utm.labs.pr.email;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.mail.internet.MimeUtility;

import md.utm.labs.pr.email.util.EmailEntity;
import md.utm.labs.pr.email.util.EmailRepresentationsExtractor;

public class Email {

	private int id;
	private String message;
	private Map<String, String> headers = new LinkedHashMap<>();
	private List<File> attachedFiles = new ArrayList<>();
	private List<EmailEntity> emailEntities = new ArrayList<>();
	public String contentType;

	private Email() {
	}

	private Email(String message, Map<String, String> headers) {
		this.message = message;
		this.headers = headers;
		emailEntities = new EmailRepresentationsExtractor(this).getEntities();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFrom() {
		return headers.get("From");
	}

	public String getTo() {
		return headers.get("To");
	}

	public String getSubject() {
		return headers.get("Subject");
	}

	public String getDate() {
		return headers.get("Date");
	}

	public String getMessage() {
		return message;
	}

	public Map<String, String> getHeaders() {
		return Collections.unmodifiableMap(headers);
	}

	public String getHeader(String header) {
		return headers.getOrDefault(header, "");
	}

	public List<File> getFilesToAttach() {
		return Collections.unmodifiableList(attachedFiles);
	}

	public String getMessageAsText() {
		return getMessageAs("text/plain");
	}

	public String getMessageAsHtml() {
		return getMessageAs("text/html");
	}
	
	public List<EmailEntity> getAttachedFiles() {
		return emailEntities.stream()
			.filter(EmailEntity::bodyIsAttachedFile)
			.collect(Collectors.toList());
	}

	private String getMessageAs(String mediaType) {
		List<EmailEntity> attachedFiles = getAttachedFiles();
		for (EmailEntity e : emailEntities) {
			if (!attachedFiles.contains(e) && e.getHeader("Content-Type").indexOf(mediaType) >= 0)
				return e.getBody();
		}
		return "";
	}

	public boolean canGetMessageAsHtml() {
		return !getMessageAs("text/html").isEmpty();
	}

	public boolean doesNotHaveAttachments() {
		return attachedFiles.isEmpty();
	}
	
	public String getContentType() {
		return contentType;
	}

	@Override
	public String toString() {
		return String.format("Email{from=%s, to=%s, subject=%s, date=%s}", getFrom(), getTo(), getSubject(), getDate());
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((headers == null) ? 0 : headers.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Email other = (Email) obj;
		if (headers == null) {
			if (other.headers != null)
				return false;
		} else if (!headers.equals(other.headers))
			return false;
		return true;
	}

	public static Email from(String email) {
		return new Email(getBody(email), getHeaders(email));
	}

	private static Map<String, String> getHeaders(String email) {
		int bodyIndex = email.indexOf("\n\n");
		String headers = email.substring(0, bodyIndex);
		return extractHeaders(headers);
	}

	private static Map<String, String> extractHeaders(String headers) {
		String[] headerTokens = headers.split("\n");
		try {
			Map<String, String> map = new LinkedHashMap<>(16, 0.75f, false);
			String previousKey = "";
			for (String token : headerTokens) {
				if (Character.isUpperCase(token.charAt(0))) {
					int colonIndex = token.indexOf(": ");
					if (colonIndex > 0) {
						String[] headerData = { token.substring(0, colonIndex),
								token.substring(colonIndex + 2, token.length()) };
						headerData[1] = MimeUtility.decodeText(headerData[1]);
						map.put(headerData[0], headerData[1]);
						previousKey = headerData[0];
					}
				} else {
					String headerValue = map.get(previousKey);
					if (headerValue != null) {
						headerValue += token;
						map.put(previousKey, headerValue);
					}
				}
			}
			return map;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	private static String getBody(String email) {
		try {
			int bodyIndex = email.indexOf("\n\n");
			return MimeUtility.decodeText(email.substring(bodyIndex + 1, email.lastIndexOf(".")));
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	public static EmailBuilder newBuilder() {
		return new EmailBuilder();
	}

	public static class EmailBuilder {
		private Email email = new Email();

		public EmailBuilder setFrom(String from) {
			email.headers.put("From", from);
			return this;
		}

		public EmailBuilder setTo(String to) {
			email.headers.put("To", to);
			return this;
		}

		public EmailBuilder setSubject(String subject) {
			email.headers.put("Subject", subject);
			return this;
		}

		public EmailBuilder setMessage(String message) {
			email.message = message;
			return this;
		}

		public EmailBuilder attachFile(File file) {
			email.attachedFiles.add(file);
			return this;
		}

		public EmailBuilder setAttachedFiles(List<File> attachedFiles) {
			email.attachedFiles.addAll(attachedFiles);
			return this;
		}

		public EmailBuilder setContentType(String contentType) {
			email.contentType = contentType;
			return this;
		}
		
		public Email build() {
			email.emailEntities = new EmailRepresentationsExtractor(email).getEntities();
			return email;
		}
	}
}
