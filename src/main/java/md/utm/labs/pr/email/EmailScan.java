package md.utm.labs.pr.email;

public class EmailScan {
	private final int index;
	private final int size;

	public EmailScan(int index, int size) {
		this.index = index;
		this.size = size;
	}

	public int getIndex() {
		return index;
	}

	public int getSize() {
		return size;
	}

	@Override
	public String toString() {
		return String.format("EmailScan{index=%s, size=%s}", index, size);
	}
}
