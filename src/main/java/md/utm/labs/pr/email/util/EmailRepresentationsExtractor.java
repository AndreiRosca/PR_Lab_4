package md.utm.labs.pr.email.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import md.utm.labs.pr.email.Email;

public class EmailRepresentationsExtractor {
	private final Email email;
	
	public EmailRepresentationsExtractor(Email email) {
		this.email = email;
	}
	
	public List<EmailEntity> getEntities() {
		List<EmailEntity> entities;
		if (emailHasBoundedRepresentations())
			entities = extractEmailRepresentations(email.getMessage(), getBoundaryToken(email.getHeaders().get("Content-Type")));
		else
			entities = wrapEmailBodyAsAnEmailEntity();
		return decodeEntities(entities);
	}
	
	private List<EmailEntity> extractEmailRepresentations(String body, String boundary) {
		BoundedBodyExtractor extractor = new BoundedBodyExtractor(body, boundary);
		List<String> representations = extractor.extract();
		List<EmailEntity> entities = representations.stream()
				.map(this::buildEntityFrom)
				.collect(Collectors.toList());
		return flatMapRepresentations(entities);
	}

	private List<EmailEntity> decodeEntities(List<EmailEntity> entities) {
		entities.forEach(EmailEntity::decodeEntityBody);
		return entities;
	}

	private List<EmailEntity> flatMapRepresentations(List<EmailEntity> representations) {
		List<EmailEntity> entities = new ArrayList<>();
		for (EmailEntity e : representations) {
			if (entityIsMultipartAlternative(e)) {
				String boundary = getBoundaryToken(e.getHeader("Content-Type"));
				entities.addAll(extractEmailRepresentations(e.getBody(), boundary));
			} else
				entities.add(e);
		}
		return entities;
	}

	private boolean entityIsMultipartAlternative(EmailEntity e) {
		return e.getHeader("Content-Type").indexOf("multipart/alternative") >= 0;
	}

	private EmailEntity buildEntityFrom(String representation) {
		String headers = representation.substring(0, representation.indexOf("\n\n"));
		String body = representation.substring(representation.indexOf("\n\n") + 1, representation.length());
		return EmailEntity.newBuilder()
				.setHeaders(extractHeaders(headers))
				.setBody(body)
				.build();
	}

	private Map<String, String> extractHeaders(String headers) {
		String[] lines = headers.split("\n");
		Map<String, String> headerMap = new HashMap<>();
		for (String line : lines) {
			String[] tokens = line.split(": ");
			headerMap.put(tokens[0], tokens[1]);
		}
		return headerMap;
	}

	private List<EmailEntity> wrapEmailBodyAsAnEmailEntity() {
		EmailEntity entity = EmailEntity.newBuilder()
				.addHeader("Content-Type", email.getHeader("Content-Type"))
				.addHeader("Content-Transfer-Encoding", email.getHeader("Content-Transfer-Encoding"))
				.setBody(email.getMessage())
				.build();
		return Arrays.asList(entity);
	}

	private boolean emailHasBoundedRepresentations() {
		return email.getHeaders().getOrDefault("Content-Type", "").indexOf("boundary=") > 0;
	}
	
	private String getBoundaryToken(String contentType) {
		String boundaryToken = "boundary=";
		int boundaryValueIndex = contentType.indexOf(boundaryToken) + boundaryToken.length();
		String boundary = contentType.substring(boundaryValueIndex, contentType.length());
		return removeQuotes(boundary);
	}

	private String removeQuotes(String token) {
		return token.replace("\"", "").replace("\'", "");
	}
}
