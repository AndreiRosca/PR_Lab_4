package md.utm.labs.pr.email;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;

public class EmailClient {
	private String smtpAddress;
	private String pop3Address;
	private String username;
	private String password;
	private int smtpPort = 25;
	private int pop3Port = 110;
	private ExecutorService executor;

	private EmailClient() {
	}

	public String getSmtpAddress() {
		return smtpAddress;
	}

	public String getPop3Address() {
		return pop3Address;
	}

	public String getUsername() {
		return username;
	}

	public String getPassword() {
		return password;
	}

	public int getSmtpPort() {
		return smtpPort;
	}

	public int getPop3Port() {
		return pop3Port;
	}

	public ExecutorService getExecutor() {
		return executor;
	}

	public CompletableFuture<String> getEmail(int emailIndex) {
		return new RetrieveEmailTask(this, emailIndex).execute();
	}

	public CompletableFuture<List<EmailScan>> getEmails() {
		return new EmailListRetriever(this).execute();
	}
	
	public void sendEmail(Email email) {
		new SendEmailTask(this, email).execute();
	}

	public static EmailClientBuilder newBuilder() {
		return new EmailClientBuilder();
	}

	public static class EmailClientBuilder {
		private EmailClient client = new EmailClient();

		public EmailClientBuilder setSmtpAddress(String smtpAddress) {
			client.smtpAddress = smtpAddress;
			return this;
		}

		public EmailClientBuilder setPop3Address(String pop3Address) {
			client.pop3Address = pop3Address;
			return this;
		}

		public EmailClientBuilder setUsername(String username) {
			client.username = username;
			return this;
		}

		public EmailClientBuilder setPassword(String password) {
			client.password = password;
			return this;
		}
		
		public EmailClientBuilder setSmtpPort(int smtpPort) {
			client.smtpPort = smtpPort;
			return this;
		}
		
		public EmailClientBuilder setPop3Port(int pop3Port) {
			client.pop3Port = pop3Port;
			return this;
		}
		
		public EmailClientBuilder setExecutor(ExecutorService executor) {
			client.executor = executor;
			return this;
		}

		public EmailClient build() {
			return client;
		}
	}
}
