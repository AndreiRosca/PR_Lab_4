package md.utm.labs.pr.email.util;

import java.util.ArrayList;
import java.util.List;

public class BoundedBodyExtractor {

	private final String body;
	private final String boundary;
	
	public BoundedBodyExtractor(String body, String boundary) {
		this.body = body;
		this.boundary = boundary;
	}
	
	public List<String> extract() {
		List<String> representations = new ArrayList<>();
		int boundaryLookupIndex = 0;
		while (bodyHasMoreBoundariesAfter(boundaryLookupIndex, body)) {
			int start = body.indexOf("--" + boundary, boundaryLookupIndex);
			int end = body.indexOf("--" + boundary, start + 1);
			boundaryLookupIndex = end;
			String representation = getRepresentationBetween(start, end);
			representations.add(representation);
		}
		return representations;
	}
	
	private boolean bodyHasMoreBoundariesAfter(int lookupIndex, String body) {
		int foundBoundary = body.indexOf("--" + boundary, lookupIndex);
		if (foundBoundary >= 0) {
			String boundary = body.substring(foundBoundary, body.indexOf("\n", foundBoundary));
			if (boundary.endsWith("--"))
				return false;
			else
				return true;
		}
		return false;
	}
	
	private String getRepresentationBetween(int start, int end) {
		int boundaryTokenLength = ("--" + boundary + "\n").length();
		String entity = body.substring(start + boundaryTokenLength, end);
		return entity;
	}
}
