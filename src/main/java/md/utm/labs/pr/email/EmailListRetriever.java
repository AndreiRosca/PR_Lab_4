package md.utm.labs.pr.email;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

public class EmailListRetriever extends EmailTask<List<EmailScan>> {
	
	public EmailListRetriever(EmailClient client) {
		super(client);
	}

	@Override
	protected void executePop3Command(BufferedReader reader, PrintWriter out) throws IOException {
		write(out, "LIST");
		readStatusLine(reader);
		List<EmailScan> emails = extractEmailList(reader);
		future.complete(emails);
	}
	
	private List<EmailScan> extractEmailList(BufferedReader reader) throws IOException {
		List<EmailScan> messages = new ArrayList<>();
		while (true) {
			String message = reader.readLine();
			if (message.equals("."))
				break;
			String[] tokens = message.split(" ");
			EmailScan scan = new EmailScan(Integer.valueOf(tokens[0]), Integer.valueOf(tokens[1]));
			messages.add(scan);
		}
		return messages;
	}
}
