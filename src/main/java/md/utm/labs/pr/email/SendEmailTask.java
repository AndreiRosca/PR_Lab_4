package md.utm.labs.pr.email;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Base64;
import java.util.Date;
import java.util.UUID;

import md.utm.labs.pr.mime.MimeUtil;

public class SendEmailTask implements Runnable {

	private final EmailClient client;
	private final Email email;

	public SendEmailTask(EmailClient client, Email email) {
		this.client = client;
		this.email = email;
	}

	public void execute() {
		client.getExecutor().submit(this);
	}

	@Override
	public void run() {
		try (Socket socket = new Socket(client.getSmtpAddress(), client.getSmtpPort())) {
			PrintWriter out = new PrintWriter(socket.getOutputStream());
			try (BufferedReader reader = 
					new BufferedReader(new InputStreamReader(socket.getInputStream()))) {
				readStatusLine(reader);
				write(out, "EHLO " + "Vasya");
				write(out, "AUTH LOGIN");
				String line;
				do {
					line = readStatusLine(reader);
				} while (line.startsWith("250"));
				write(out, new String(Base64.getEncoder().encode(client.getUsername().getBytes())));
				readStatusLine(reader);
				write(out, new String(Base64.getEncoder().encode(client.getPassword().getBytes())));
				readStatusLine(reader);
				write(out, "MAIL FROM: " + email.getFrom());
				readStatusLine(reader);
				write(out, "RCPT TO: " + email.getTo());
				readStatusLine(reader);
				write(out, "DATA");
				readStatusLine(reader);
				writeEmailBody(out);
				readStatusLine(reader);
				write(out, "QUIT");
			} finally {
				out.close();
			}
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	private void writeEmailBody(PrintWriter out) {
		if (email.doesNotHaveAttachments())
			writePlainEmailBody(out);
		else
			writeEmailBodyWithAttachments(out);
	}
	
	private void writeEmailBodyWithAttachments(PrintWriter out) {
		UUID uuid = UUID.randomUUID();
		UUID bodyBoundary = UUID.randomUUID();
		write(out, "From: " + email.getFrom());
		write(out, "To: " + email.getTo());
		write(out, "Subject: " + email.getSubject());
		write(out, "Date: " + new Date());
		write(out, "Content-Type: multipart/mixed; boundary=\"" + uuid + "\"");
		write(out, "");
		write(out, "--" + uuid);
		write(out, "Content-Type: multipart/alternative; boundary=\"" + bodyBoundary + "\"");
		write(out, "");
		write(out, "--" + bodyBoundary);
		write(out, "Content-Type: " + email.getContentType() + "; charset=\"UTF-8\"");
		write(out, "Content-Transfer-Encoding: 8bit");
		write(out, "");
		write(out, email.getMessage());
		write(out, "");
		write(out, "--" + bodyBoundary + "--");
		write(out, "");
		write(out, "--" + uuid);
		MimeUtil util = new MimeUtil();
		int fileIndex = 0;
		for (File attachedFile : email.getFilesToAttach()) {
			String fileMime = util.detectMimeType(attachedFile);
			write(out, "Content-Type: " + fileMime + "; name=\"" + attachedFile.getName() + "\"");
			write(out, "Content-Disposition: attachment; filename=\"" + attachedFile.getName() + "\"");
			write(out, "Content-Transfer-Encoding: base64");
			write(out, "");
			write(out, getFileBodyAsBase64(attachedFile));
			write(out, "");
			if (fileIndex == email.getFilesToAttach().size() - 1)
				write(out, "--" + uuid + "--");
			else
				write(out, "--" + uuid);
			++fileIndex;
		}
		writeEmailEnd(out);
	}

	private String getFileBodyAsBase64(File file) {
		ByteArrayOutputStream fileContents = new ByteArrayOutputStream();
		try (BufferedInputStream in = new BufferedInputStream(new FileInputStream(file))) {
			byte[] buffer = new byte[4096];
			int bytesRead;
			while ((bytesRead = in.read(buffer)) != -1) 
				fileContents.write(buffer, 0, bytesRead);
			byte[] encodedFile = Base64.getEncoder().encode(fileContents.toByteArray());
			return transformBase64String(new String(encodedFile));
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	private String transformBase64String(String base64Body) {
		final int CHUNK_SIZE = 4096;
		int parts = base64Body.length() / CHUNK_SIZE;
		int remainingChars = base64Body.length() % CHUNK_SIZE;
		StringBuilder result = new StringBuilder();
		for (int i = 0; i < parts; ++i) {
			result.append(base64Body.substring(i * CHUNK_SIZE, i * CHUNK_SIZE + CHUNK_SIZE));
			result.append("\r\n");
		}
		result.append(base64Body.substring(parts * CHUNK_SIZE, parts * CHUNK_SIZE + remainingChars));
		result.append("\r\n");
		return result.toString();
	}

	private void writePlainEmailBody(PrintWriter out) {
		UUID uuid = UUID.randomUUID();
		write(out, "From: " + email.getFrom());
		write(out, "To: " + email.getTo());
		write(out, "Subject: " + email.getSubject());
		write(out, "Date: " + new Date());
		write(out, "Content-Type: multipart/alternative; boundary=\"" + uuid + "\"");
		write(out, "");
		write(out, "--" + uuid);
		write(out, "Content-Type: " + email.getContentType() + "; charset=\"UTF-8\"");
		write(out, "Content-Transfer-Encoding: 8bit");
		write(out, "");
		write(out, email.getMessage());
		write(out, "");
		write(out, "--" + uuid + "--");
		writeEmailEnd(out);
	}

	private void writeEmailEnd(PrintWriter out) {
		write(out, "");
		write(out, ".");
		write(out, "");
	}

	private void write(PrintWriter out, String message) {
		out.print(message + "\r\n");
		out.flush();
	}
	
	private String readStatusLine(BufferedReader reader) throws IOException {
		String line = reader.readLine();
		return line;
	}
}
