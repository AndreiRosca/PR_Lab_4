package md.utm.labs.pr.email;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;

public class RetrieveEmailTask extends EmailTask<String> {
	private final int emailIndex;

	public RetrieveEmailTask(EmailClient client, int emailIndex) {
		super(client);
		this.emailIndex = emailIndex;
	}
	
	@Override
	protected void executePop3Command(BufferedReader reader, PrintWriter out) throws IOException {
		write(out, "RETR " + emailIndex);
		readStatusLine(reader);
		retrieveEmailBody(reader);
	}
	
	private void retrieveEmailBody(BufferedReader reader) throws IOException {
		StringBuilder email = new StringBuilder();
		while (true) {
			String line = reader.readLine();
			email.append(line).append("\n");
			String end = email.substring(email.length() - 3, email.length());
			if(end.endsWith("\n.\n"))
				break;
		}
		future.complete(email.toString());
	}
}
