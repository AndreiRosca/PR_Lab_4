package md.utm.labs.pr.email.util;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

import javax.mail.internet.MimeUtility;

public class EmailEntity {
	private Map<String, String> headers = new HashMap<>();
	private String body;

	private EmailEntity() {
	}

	public Map<String, String> getHeaders() {
		return headers;
	}

	public String getHeader(String header) {
		return headers.getOrDefault(header, "");
	}

	public String getBody() {
		return body;
	}

	public void decodeEntityBody() {
		if (bodyIsAttachedFile())
			return;
		if (bodyIsQuotedPrintable())
			body = decodeQuotedPrintable();
		else if (bodyIsBase64Encoded())
			body = decodeBase64Body();
	}

	public byte[] getAttachedFileContent() {
		if (bodyIsAttachedFile()) {
			body = body.replace("\n", "");
			return Base64.getDecoder().decode(body.getBytes());
		}
		throw new EmailEntityIsNotAttachedFileException();
	}

	public String getAttachedFileName() {
		String contentDisposition = getHeader("Content-Disposition");
		return getFileName(contentDisposition);
	}

	private String getFileName(String contentDisposition) {
		String fileNameToken = "filename=";
		int start = contentDisposition.indexOf(fileNameToken);
		int end = contentDisposition.length();
		String fileName = contentDisposition.substring(start + fileNameToken.length(), end);
		return removeQuotes(fileName);
	}

	private String removeQuotes(String fileName) {
		return fileName.replace("\"", "").replace("\'", "");
	}

	public boolean bodyIsAttachedFile() {
		return getHeader("Content-Disposition").indexOf("attachment") >= 0 && bodyIsBase64Encoded();
	}

	private String decodeBase64Body() {
		body = body.replace("\n", "");
		return new String(Base64.getDecoder().decode(body.getBytes()));
	}

	private boolean bodyIsBase64Encoded() {
		return headers.getOrDefault("Content-Transfer-Encoding", "").equalsIgnoreCase("base64");
	}

	private boolean bodyIsQuotedPrintable() {
		return headers.getOrDefault("Content-Transfer-Encoding", "").equalsIgnoreCase("quoted-printable");
	}

	private String decodeQuotedPrintable() {
		try {
			ByteArrayInputStream bodyAsStream = new ByteArrayInputStream(body.getBytes());
			InputStream stream = MimeUtility.decode(bodyAsStream, "quoted-printable");
			return getStreamAsString(stream);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	private String getStreamAsString(InputStream stream) throws IOException {
		try (BufferedReader reader = new BufferedReader(new InputStreamReader(stream))) {
			return reader.lines().collect(Collectors.joining("\n"));
		}
	}

	@Override
	public String toString() {
		return String.format("EmailEntity{headers=%s, body=%s}", headers, body);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((body == null) ? 0 : body.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EmailEntity other = (EmailEntity) obj;
		if (body == null) {
			if (other.body != null)
				return false;
		} else if (!body.equals(other.body))
			return false;
		return true;
	}

	public static EmailEntityBuilder newBuilder() {
		return new EmailEntityBuilder();
	}

	public static class EmailEntityBuilder {
		private EmailEntity emailEntity = new EmailEntity();

		public EmailEntityBuilder setBody(String body) {
			emailEntity.body = body;
			return this;
		}

		public EmailEntityBuilder addHeader(String key, String value) {
			emailEntity.headers.put(key, value);
			return this;
		}

		public EmailEntityBuilder setHeaders(Map<String, String> headers) {
			emailEntity.headers = headers;
			return this;
		}

		public EmailEntity build() {
			return emailEntity;
		}
	}

	public static class EmailEntityIsNotAttachedFileException extends RuntimeException {
		private static final long serialVersionUID = 1L;

		public EmailEntityIsNotAttachedFileException() {
		}

		public EmailEntityIsNotAttachedFileException(String message) {
			super(message);
		}
	}
}
