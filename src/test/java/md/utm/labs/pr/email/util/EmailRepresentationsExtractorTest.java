package md.utm.labs.pr.email.util;

import static md.utm.labs.pr.email.TestUtil.getFileContents;
import static org.junit.Assert.*;

import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;

import md.utm.labs.pr.email.Email;

public class EmailRepresentationsExtractorTest {
	private static Email gmailLetter;
	private static Email dzoneLetter;
	private static Email base64Letter;
	private static Email linkedInLetter;
	private static Email emailWithAttchments;
	private static Email syncfusionLetter;

	@BeforeClass
	public static void setUp() {
		gmailLetter = Email.from(getFileContents("mail1.txt"));
		dzoneLetter = Email.from(getFileContents("mail2.txt"));
		base64Letter = Email.from(getFileContents("mail3.txt"));
		linkedInLetter = Email.from(getFileContents("mail4.txt"));
		emailWithAttchments = Email.from(getFileContents("mail5.txt"));
		syncfusionLetter = Email.from(getFileContents("mail6.txt"));
	}
	
	@Test
	public void canDecodeAnEmailWithoutMultipleRepresentations() {
		EmailRepresentationsExtractor extractor = new EmailRepresentationsExtractor(syncfusionLetter);
		List<EmailEntity> entities = extractor.getEntities();
		assertEquals(1, entities.size());
		EmailEntity entity = entities.get(0);
		entity.decodeEntityBody();
		assertTrue(entity.getBody().contains("<div name=\"quote\" style=\"margin:10px 5px 5px 10px; "));
	}
	
	@Test
	public void canDecodeAttachedFile() {
		EmailRepresentationsExtractor extractor = new EmailRepresentationsExtractor(emailWithAttchments);
		List<EmailEntity> entities = extractor.getEntities();
		assertEquals(3, entities.size());
		EmailEntity textEntity = entities.get(2);
		assertTrue(textEntity.getBody().contains("http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd"));
	}
}
