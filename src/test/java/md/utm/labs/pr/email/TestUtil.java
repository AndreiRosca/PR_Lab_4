package md.utm.labs.pr.email;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.stream.Collectors;

public class TestUtil {
	public static String getFileContents(String fileName) {
		try (BufferedReader reader = new BufferedReader(new InputStreamReader(
				EmailTest.class.getResourceAsStream("/" + fileName)))) {
			return reader.lines().collect(Collectors.joining("\n"));
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
}
