package md.utm.labs.pr.email;

import static md.utm.labs.pr.email.TestUtil.getFileContents;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;

import org.junit.BeforeClass;
import org.junit.Test;

public class EmailTest {

	private static Email gmailLetter;
	private static Email dzoneLetter;
	private static Email base64Letter;
	private static Email linkedInLetter;
	private static Email syncfusionLetter;

	@BeforeClass
	public static void setUp() {
		gmailLetter = Email.from(getFileContents("mail1.txt"));
		dzoneLetter = Email.from(getFileContents("mail2.txt"));
		base64Letter = Email.from(getFileContents("mail3.txt"));
		linkedInLetter = Email.from(getFileContents("mail4.txt"));
		syncfusionLetter = Email.from(getFileContents("mail6.txt"));
	}

	@Test
	public void canViewASimpleGmailLetterAsPlainText() throws IOException {
		String targetBody = "\nThis is an example email from gmail.\n\nKind regards,\n\nMike Smith\n\n";
		assertEquals(targetBody, gmailLetter.getMessageAsText());
	}

	@Test
	public void canViewADzoneLetterAsPlainText() {
		String targetBody = getFileContents("mail2PlainTextBody.txt");
		assertEquals(targetBody, dzoneLetter.getMessageAsText());
	}

	@Test
	public void canViewBase64LetterAsPlainText() {
		base64Letter.getMessageAsText();
		assertTrue(base64Letter.getMessageAsText().contains("Oracle Corporation"));
	}

	@Test
	public void canViewADzoneLetterAsHtml() {
		String targetBody = getFileContents("mail2HtmlBody.txt");
		assertEquals(targetBody, dzoneLetter.getMessageAsHtml());
	}

	@Test
	public void canCheckIfCanViewADzoneLetterAsHtml() {
		assertTrue(dzoneLetter.canGetMessageAsHtml());
	}

	@Test
	public void canViewAQuotedPritableHtmlEmail() {
		String body = linkedInLetter.getMessageAsHtml();
		assertTrue(body.contains("<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" "));
	}

	@Test
	public void canViewAnEmailWithNoBoundaryToken() {
		String body = syncfusionLetter.getMessageAsHtml();
		assertTrue(body.contains("<div name=\"quote\" style=\"margin:10px 5px 5px 10px; "));
	}
}
